import React, { Component } from 'react';
import './App.scss';
import CalendarContainer from 'containers/calendar';

class App extends Component {
  render() {
	return (
	  <div>
		<CalendarContainer />
	  </div>
	);
  }
}

export default App;
