import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux'
import configureStore from './utils/store';
import App from './App';
import * as registerServiceWorker from './utils/serviceWorker';

ReactDOM.render(
  <Provider store={configureStore()}>
	<App />
  </Provider>,
  document.getElementById('root')
);
// Learn more about service workers: http://bit.ly/CRA-PWA
registerServiceWorker.register();
