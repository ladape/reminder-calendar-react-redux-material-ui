import { combineReducers } from 'redux';
import reminderReducer from './ReminderReducer';
import testReducer from './TestReducer';

export default combineReducers({
  reminderReducer,
  testReducer
});
