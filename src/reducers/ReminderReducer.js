export default (state = {reminders: []}, {type='', payload = ''} ) => {
  switch (type) {
	case 'REMINDERS_GET':
	  return {
		...state,
		reminders: payload
	  };
	case 'REMINDER_UPDATE':
	  return {
		...state,
		reminders: payload
	  };
	case 'REMINDER_CREATE':
	  const {reminders} = state;

	  reminders.push(payload);

	  return {
		...state,
		reminders
	  };
	case 'REMINDER_DELETE': // todo create id
	  return {
		...state,
		reminders: payload
	  };
	default:
	  return state;
  }
}
