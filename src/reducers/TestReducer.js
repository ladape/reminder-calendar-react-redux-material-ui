export default (state = {}, {type='', payload = ''} ) => {
  switch (type) {
	case 'TEST_ACTION_GET':
	  return {
	    ...state,
		test: payload
	  };
	case 'TEST_ACTION_UPDATE':
	  return {
		...state,
		test: payload
	  };
	default:
	  return state;
  }
}
