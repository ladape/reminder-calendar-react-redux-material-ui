import React, { Component } from 'react';
import { connect } from 'react-redux';
import './calendar.scss';
import {remindersGet, reminderUpdate} from "actions/ReminderAction";
import CalendarMonth from "./components/CalendarMonth/CalendarMonth";
import moment from 'moment';
import CalendarService from './services/CalendarService'
import {store} from 'utils/store';

class CalendarContainer extends Component {
  dayNames = [ 'Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday' ];
  state = {
    showDateYearMonth: moment(),
	currentDate: moment(),
	dayCells: []
  };

  componentDidMount() {
    CalendarService.getReminders();

	this._handleDayCellsByYearAndMonth();
  }

  _handleDayCellsByYearAndMonth(year = this.state.showDateYearMonth.year(), month = this.state.showDateYearMonth.format('MM')) {
    const dayCells = this.getDayCellsByYearAndMonth(year, month);
    const showDateYearMonth = moment(`${year}-${month}`);

	this.setState({
	  dayCells: dayCells,
	  showDateYearMonth: showDateYearMonth
	});
  }

  getDayCellsByYearAndMonth(year, month) {
	const globalState = store.getState();
	const reminders = globalState.reminderReducer.reminders;
	const date = moment(`${year}-${month}`);
	const firstDateOfMonth = moment(`${year}-${month}-01`);
	const nthDayTheFirstOfMonthOfWeek = this.dayNames.indexOf(firstDateOfMonth.format('dddd')) + 1;
	const numberOfCells = nthDayTheFirstOfMonthOfWeek + date.daysInMonth();
	const dayCells = [];

	for(let i = 1; i < numberOfCells; i++) {
	  const dayIndex = i <= nthDayTheFirstOfMonthOfWeek - 1 ? 0 : i - nthDayTheFirstOfMonthOfWeek + 1;
	  const day = dayIndex < 10 ? '0' + dayIndex : dayIndex; // its for fix a moment warning
	  const relevantDay = moment(`${year}-${month}-${day}`);
	  const isToday = relevantDay.isSame(this.state.currentDate, 'day');
	  const date = i <= nthDayTheFirstOfMonthOfWeek - 1 ? null :  relevantDay;
	  const relevantReminders = reminders.filter((reminder)=> {
		return date && date.isSame(reminder.date, 'day');
	  });

	  dayCells.push({
		date,
		index: dayIndex,
		isToday: isToday,
		reminders: relevantReminders
	  });
	}

	return dayCells;
  }

  prevMonth = () => {
	const prevMonth = this.state.showDateYearMonth.subtract(1, "M");

	this._handleDayCellsByYearAndMonth(prevMonth.year(), prevMonth.format('MM'));
  };
  nextMonth = () => {
	const prevMonth = this.state.showDateYearMonth.add(1, "M");

	this._handleDayCellsByYearAndMonth(prevMonth.year(), prevMonth.format('MM'));
  };

  addReminder = (reminder) => {
	CalendarService.createReminder(reminder);
	this._handleDayCellsByYearAndMonth(reminder.date.year(), reminder.date.format('MM'));
  };
  removeReminder = (reminder) => {
	CalendarService.deleteReminder(reminder);
	this._handleDayCellsByYearAndMonth(reminder.date.year(), reminder.date.format('MM'));
  };

  render() {
	return (
	  <div className="calendar-wrapper">
		<CalendarMonth
		  {...this.props}
		  currentDate={this.state.currentDate}
		  showDateYearMonth={this.state.showDateYearMonth}
		  dayCells={this.state.dayCells}
		  prevMonth={this.prevMonth}
		  nextMonth={this.nextMonth}
		  dayNames={this.dayNames}
		  addReminder={this.addReminder}
		  removeReminder={this.removeReminder}
		/>
	  </div>
	);
  }
}

const mapStateToProps = ({reminderReducer}) => ({
  ...reminderReducer
});
const mapDispatchProps = {
  remindersGet,
  reminderUpdate
};

export default connect(mapStateToProps, mapDispatchProps)(CalendarContainer);
