import {store} from 'utils/store';
import moment from 'moment';
import {remindersGet, reminderCreate, reminderUpdate, reminderDelete} from 'actions/ReminderAction';

class CalendarService {
  getReminders() {
    const reminders = [
	  {
		category: 'normal',
		title: 'fromServiceReminder',
		time: '18:00',
		date: moment()
	  },
	  {
		category: 'normal',
		title: 'fromServiceReminder',
		time: '19:00',
		date: moment()
	  }
	];

	store.dispatch(remindersGet(reminders));
  }
  createReminder(reminder) {
	store.dispatch(reminderCreate(reminder));
  }
  updateReminder(reminder) {
	store.dispatch(reminderUpdate(reminder));
  }
  deleteReminder(reminder) {
	store.dispatch(reminderDelete(reminder));
  }
}

export default new CalendarService();
