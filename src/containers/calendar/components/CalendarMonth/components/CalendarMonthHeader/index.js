import React, {Component} from 'react';
import PropTypes from 'prop-types';
import './CalendarMonthHeader.scss'

class CalendarMonthHeader extends Component {
  render() {
	const {dayNames} = this.props;

	return (
	  <div>
		<div className='dayNames'>
		  {dayNames.map((dayName, index)=> (
			  <div className='dayName' key={index}>{dayName}</div>
			)
		  )}
		</div>
	  </div>
	);
  }
}

CalendarMonthHeader.propTypes = {};

export default CalendarMonthHeader;
