import React, {Component} from 'react';
import './day.scss';
import PropTypes from 'prop-types';
import ReminderList from "./components/ReminderList";
import ReminderForm from "./components/ReminderForm";
import Icon from '@material-ui/core/Icon';
import { withStyles } from '@material-ui/core/styles';
import red from '@material-ui/core/colors/red';

const styles = theme => ({
  root: {
	display: 'flex',
	justifyContent: 'center',
	alignItems: 'flex-end',
  },
  icon: {
	margin: theme.spacing.unit * 2,
  },
  iconHover: {
	margin: theme.spacing.unit * 2,
	'&:hover': {
	  color: red[800],
	},
  },
});

class Day extends Component {
  state = {
    hasOpenedReminderForm: false
  };

  setHasOpenedReminderForm = (isOpen)=> {
    this.setState({
	  hasOpenedReminderForm: isOpen
	});
  };

  render() {
    const {day} = this.props;
    const todayClassName = day.isToday ? 'today' : '';

	return (
	  <div className='dayCell-wrapper'>
		<div className={'dayCell ' + todayClassName}>
		  { day.index }
		  <button className='addReminder' onClick={()=> {this.setHasOpenedReminderForm(true)}}>
			<Icon color="primary">+</Icon>
		  </button>
		  <ReminderList {...this.props} reminders={day.reminders} />
		  <ReminderForm {...this.props} isOpened={this.state.hasOpenedReminderForm} setHasOpenedReminderForm = {this.setHasOpenedReminderForm} />
		</div>
	  </div>
	);
  }
}

Day.propTypes = {};

export default withStyles(styles)(Day);
