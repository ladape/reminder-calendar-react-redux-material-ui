import React, {Component} from 'react';
import './reminder.scss';
import {Delete, Edit} from '@material-ui/icons'
import PropTypes from 'prop-types';

class ReminderList extends Component {
  delete() {}
  edit() {}

  render() {
    const {reminders} = this.props;

	return (
	  <div className='reminder-list'>
		{reminders.map((reminder, index)=> (
			<div key={index}>
			  <div className={'reminder-wrapper ' + reminder.category}>
				<div className='reminderTitle'>{reminder.title}</div>
				<div className='reminderEdit clickable' onClick={this.edit}>
				  <Edit />
				</div>
				<div className='reminderDelete clickable' onClick={this.delete}><Delete/></div>
			  </div>
			</div>
		))}
	  </div>
	);
  }
}

ReminderList.propTypes = {};

export default ReminderList;
