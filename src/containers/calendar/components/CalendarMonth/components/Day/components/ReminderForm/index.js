import React, {Component} from 'react';
import './reminderForm.scss';
import PropTypes from 'prop-types';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Select from '@material-ui/core/Select';
import MenuItem from "@material-ui/core/es/MenuItem/MenuItem";
import InputLabel from "@material-ui/core/es/InputLabel/InputLabel";

class ReminderForm extends Component {
  state = {
    category: 'normal',
	title: 'Reminder',
	time: '8:00',
	date: this.props.day.date
  };
  reminderTypes = ['normal', 'important', 'high'];
  handleSubmit = event => {
	event.preventDefault();

	const { category, title, time, date } = this.state;

	const updatedReminder = {
	  category: category,
	  title: title,
	  time: time,
	  date: date
	};

	this.props.addReminder(updatedReminder);
	this.props.setHasOpenedReminderForm(false);
  };
  handleChange = (e) => {
	const {name, value} = e.target;

    this.setState(
	  {[name]: value}
	);
  };
  render() {
    let {isOpened, setHasOpenedReminderForm} = this.props;

    if (!isOpened) {
      return null;
	}

	return (
	  <div>
		<div className='reminder-form-wrapper'>
		  <form className="reminder-form" onSubmit={this.handleSubmit}>
			<div className='close' onClick={()=> {setHasOpenedReminderForm(false)} }>×</div>
			<div className='fullWidth'>
			  <TextField
				type="text"
				placeholder='Reminder name.'
				autoFocus={true}
				value={this.state.title}
				onChange={this.handleChange}
				name='title'
				inputProps={{
				  maxLength: 30
				}}
			  />
			</div>
			<div className='fullWidth'>
			  <InputLabel htmlFor="age-simple">Category</InputLabel>
			</div>
			<div className='fullWidth'>
			  <Select
				onChange={this.handleChange}
				name='category'
				value={this.state.category}
				inputProps={{
				  name: 'category',
				  className: 'category-select',
				}}
			  >
				{
				  this.reminderTypes.map((val, index)=>
					<MenuItem key={index} value={val}>{val}</MenuItem>
				  )}
			  </Select>
			</div>
			<div className='fullWidth'>
			  <TextField
				name='time'
				label="Reminder time"
				type="time"
				defaultValue="07:30"
				className='reminderTimePicker'
				onChange={this.handleChange}
				InputLabelProps={{
				  shrink: true,
				}}
				inputProps={{
				  step: 300, // 5 min
				}}
			  />
			</div>
			<div className='fullWidth'>
			  <Button type="submit"
					  onClick={this.props.onDelete}
					  variant="contained"
					  color="secondary">
				remove
			  </Button>
			  <Button type="submit" variant="contained" color="primary">
				save
			  </Button>
			</div>
		  </form>
		</div>
	  </div>
	);
  }
}

ReminderForm.propTypes = {};

export default ReminderForm;
