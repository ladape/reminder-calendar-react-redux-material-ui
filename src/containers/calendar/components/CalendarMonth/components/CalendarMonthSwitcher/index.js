import React, {Component} from 'react';
import PropTypes from 'prop-types';

class CalendarMonthSwitcher extends Component {
  render() {
    const {showDateYearMonth, prevMonth, nextMonth} = this.props;

	return (
	  <div>
		<button onClick={prevMonth}>-</button>
		{showDateYearMonth.format('YYYY')} - {showDateYearMonth.format('MMMM')}
		<button onClick={nextMonth}>+</button>
	  </div>
	);
  }
}

CalendarMonthSwitcher.propTypes = {};

export default CalendarMonthSwitcher;
