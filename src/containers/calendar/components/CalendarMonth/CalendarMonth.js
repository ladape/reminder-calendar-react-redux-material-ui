import React, {Component} from 'react';
import PropTypes from 'prop-types';
import CalendarMonthSwitcher from "./components/CalendarMonthSwitcher";
import Day from "./components/Day";
import CalendarMonthHeader from "./components/CalendarMonthHeader";

class CalendarMonth extends Component {
  render() {
    const {dayCells, dayNames} = this.props;

	return (
	  <div>
		<CalendarMonthSwitcher {...this.props} />
		<CalendarMonthHeader dayNames={dayNames}/>
		{dayCells.map((dayCell, index)=> {
			if (dayCell.index) {
			  return <Day key={index} day={dayCell} {...this.props} />
			} else {
			  return <div className='dayCell empty' key={index}></div>
			}
		  }
		)}
	  </div>
	);
  }
}

CalendarMonth.propTypes = {};

export default CalendarMonth;
