export const testActionGet = () => dispatch => {
  dispatch({
	type: 'TEST_ACTION_GET',
	payload: 'get'
  })
};

export const testActionUpdate = () => dispatch => {
  dispatch({
	type: 'TEST_ACTION_UPDATE',
	payload: 'update'
  })
};
