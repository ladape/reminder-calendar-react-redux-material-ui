export const remindersGet = (reminders) => dispatch => {
  dispatch({
	type: 'REMINDERS_GET',
	payload: reminders
  })
};

export const reminderCreate = (reminder) => dispatch => {
  dispatch({
	type: 'REMINDER_CREATE',
	payload: reminder
  })
};

export const reminderUpdate = (reminder) => dispatch => {
  dispatch({
	type: 'REMINDER_UPDATE',
	payload: reminder
  })
};

export const reminderDelete = () => dispatch => {
  dispatch({
	type: 'REMINDER_DELETE',
	payload: {}
  })
};
